ARG POSTGRES_VERSION=14
FROM postgres:${POSTGRES_VERSION}
RUN localedef -i fr_FR -c -f UTF-8 -A /usr/share/locale/locale.alias fr_FR.UTF-8
ENV LANG fr_FR.utf8
COPY create-db-and-users.sh /docker-entrypoint-initdb.d/
