#!/bin/bash

set -e
set -u

function create_user_and_database() {
	#local database=$1
  local database=$(echo $1 | cut -d ':' -f1)
  local user=$(echo $1 | cut -d ':' -f2)
  local passwd=$(echo $1 | cut -d ':' -f3)
  
	echo "  Creating user and database '$database'"
	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	    CREATE USER $user WITH ENCRYPTED PASSWORD '$passwd';
	    CREATE DATABASE $database;
	    GRANT ALL PRIVILEGES ON DATABASE $database TO $user;
EOSQL
}

if [ -n "$POSTGRES_MULTIPLE_DATABASES" ]; then
	echo "Multiple database creation requested: $POSTGRES_MULTIPLE_DATABASES"
	for db in $(echo $POSTGRES_MULTIPLE_DATABASES | tr ',' ' '); do
		create_user_and_database $db
	done
	echo "Multiple databases created"
fi
